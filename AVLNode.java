/**
 * File: Polish.java
 * Machine Problem # 1
 *
 * Members: Rosheil Parel
 *          Grygjeanne Grace Icay
 *          Anfernee Sodusta
 *
 * LinkedStack Implementation
 * URL: http://algs4.cs.princeton.edu/home/
 * Article Title: Algorithms, 4th Edition
 * Authors: Robert Sedgewick and Kevin Wayne
 */

/**
 * Implementation of the Doubly LinkedStack. Where the storage and linking is happening
 */
public class AVLNode <T>{
   private T  element;
   private AVLNode right;
   private AVLNode left;

   public AVLNode(){
      System.out.println("Initiate AVL");
   }

   public AVLNode(Object e, AVLNode n){
      element = e;
      next    = n;
   }

   public void rightNode(AVLNode r){
      right = r;
   }

   public void leftNode(AVLNode l){
      left = l;
   }

   public void currNode(int d){
      data = d;
   }

   public Object getData(){
      return data;
   }

   public Object getRightNode(){
      return right;
   }
   public Object getLeftNode(){
      return left;
   }

}
