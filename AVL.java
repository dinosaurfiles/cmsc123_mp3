import java.util.*;
import java.io.*;

/**
 * File: AVL.java
 * Machine Problem # 3
 *
 * Members: Mary Grygjeanne Grace Icay
            Rosheil Parel
            Anfernee Sodusta
 *
 * //searched reference
 * URL:
 * Article Title:
 * Authors:
 */

/** Main class of AVL.java */
public class AVL{
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Enter file name >> ");
        String filename = scan.nextLine();
        AVL cases    = new AVL(filename);
    }

    /**
     * Main AVL method. Gets the filename and proceeds to the converting of the tree into avl and then giving the key output
     * @param The path or the filename of the txt
     */

    public AVL(String filename){
        try{
            File file = new File(filename);
            FileReader     fr = new FileReader(file);
            BufferedReader br = new BufferedReader(fr);
            //diri dayun i-implement ang insert blah blah
            /**String infixcase  = "";
            int    expcounter = Integer.parseInt(br.readLine()) + 1;
            int    counter    = 1;

            do {
                infixcase = br.readLine();

                infixTopostfix(sanitize(infixcase), infixcase, counter);

                counter++;
            } while (counter < expcounter);
        }catch (Exception e) {}*/
        }
}
    /**INSERTING A NEW RECORD IN THE BINARY TREE:*/
    public BST_INSERT(T, K, d){
        private Node A, B, P;
        private int size = //number of input keys
        //read text file inputs in the main code execution. first line contains keys of the binary tree, each node separated by a space and second line contains the node to be searched
        //di gale dapat pag-ibutang dayun sa array kay mahimo sya sequential searching
        A = BST; //ambot kung ano ni. basta may nakabutang bi nga variable T, pero since gingamit ko ang T as tree, BST nalang ginbutang ko di
        K = //the node to be searched
        while (A != null){
            if (K == KEY(A)){
                System.out.println("Duplicate key found.");
                break;
            }
            else if (K < KEY(A)){
                B = A;
                A = LSON(A);
            }
            else if (K > KEY(A)){
                B = A;
                A = RSON(A);
            }
        //every insert sang key, i-check ya dapat kung h or h+1 ang height. dason kung indi, i-implement ang either of the rotations
        //if-else pangcheck sa height, implement ang constructor nga rotation
        }
        public void RROT(){
            B = LSON(A); //B is previously the left son of A. B becomes the root of the rebalanced tree/subtree
            LSON(A) = RSON(B);
            RSON(B)= A;
            BF(B) = 0;
            BF(A) = BF(B);
        }
        public void LROT(){
            B = RSON(A); //B is previously the right son of A. B becomes the root of the rebalanced tree/subtree
            RSON(A) = LSON(B);
            LSON(B) = A;
            BF(B) = 0;
            BF(A) = BF(B);
        }
        public void LRROT(){
            B = LSON(A);
            P = RSON(B); //P becomes the root of the rebalanced tree/subtree
            RSON(B) = LSON(P);
            LSON(P) = B;
            LSON(A) = RSON(P);
            RSON(P) = A;
            if (BF(P) == 0){
                BF(A) = 0;
                BF(B) = 0;
            }
            else if (BF(P) == 1){
                BF(A) = 0;
                BF(B) = -1;
            }
            else if (BF(P)  -1){
                BF(A) = 1;
                BF(B) = 0;
            }
            BF(P) = 0;
        }
        public void RLROT(){
            B = RSON(A);
            P = LSON(B); //becomes the root of the rebalanced tree/subtree
            LSON(B) = RSON(P);
            RSON(P) = B;
            RSON(A) = LSON(P);
            LSON(P) = A;
            if (BF(P) == 0){
                BF(A)=0;
                BF(B)=0;
            }
            else if (BF(P) == 1){
                BF(A) = -1;
                BF(B) = 0;
            }
            else if (BF(P) == -1){
                BF(A) = 0;
                BF(B) = 1;
            }
        }
    }
    /** SEARCHING THE AVL TREE*/
    public void BST_SEARCH(T,K){ //binary search tree B and key K
        A = BST;
        while (A != null){
            if(K == KEY(A)){
                System.out.println(A + "found at index" + KEY(A) + "of the AVL TREE.") //search is successful
                break;
            }
            else if(K < KEY(A)){
                A = LSON (A); //go search the left subtree
            }
            else if(K > KEY(A)){
                A = RSON(A); //go search the right subtree
            }
        }
        System.out.println("The key" + A + "was not found in the AVL tree.");//unsuccessful search;
    }
}

